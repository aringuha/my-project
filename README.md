### Java Web Services and RESTful API with Spring Boot for Movie

### What you'll need
- [Java](https://www.java.com) 1.8
- [Spring Boot](http://spring.io/projects/spring-boot) 2.1.2
- [maven](https://maven.apache.org/)
- JPA/Hibernate
- [H2](http://www.h2database.com)
- DevTools
- [Lombok](https://projectlombok.org/)
- [JUnit 4](https://junit.org/junit4/)
- [Swagger](https://swagger.io/) 2

### build with Maven & Run the application
```sh
mvn package && java -jar target/movieManager.jar
```

Database UI
http://localhost:8080/h2/

Swagger UI
http://localhost:8080/swagger-ui.html